import { Brand } from "./Brand";
import { Model } from "./Model";
import { Car } from "./Car";
import { BmwFactory } from "./BmwFactory";
import { TireSize } from "./TireSize";
import { FactoryGenerics } from "./FactoryGenerics";
//interface
const tire_1: TireSize = {
  radius: 15,
  size: {
    first: 205,
    second: 50
  },
  getTireSize(): string {
    return this.size.first + '/' + this.size.second + ' R' + this.radius;
  }
}
console.log(tire_1.getTireSize());
//Factory with generics
const factoryGen = new FactoryGenerics;
const carToyota_1 = factoryGen.create(Car);
let carToyota_2 = factoryGen.create(Car, 'Toyota', 'Supra', '280', 'Sed', 130, [0], '#11234', 'whiteblue');
const BrandMitsubishi = factoryGen.create(Brand, 'Mitsubishi');
let ModelBmwM3 = factoryGen.create(Model, 'Bmw', 'M3', '200');
console.log("Так работает фабрика с использованием generics: ");
console.log(ModelBmwM3);
console.log(carToyota_2);
carToyota_2.setCost = 150000;
console.log(carToyota_2.getPonts);
console.log(BrandMitsubishi.getNameBrand);
console.log("--------------------");
//FactoryMethod
const factory = new BmwFactory;
let ModelBmwX5 = factory.create('X5');
let ModelBmwX6 = factory.create('X6');

console.log("Так работает фабричный метод: ");
console.log(ModelBmwX5);
console.log(ModelBmwX6);
console.log("----------------------");
let modelCar1 = new Model('Bmw', 'X5', 270);
console.log(modelCar1.getSpeed);
console.log(modelCar1.getNameBrand);
//using the constructor
let car1 = new Car('Mitsubishi', 'Outlander', '190', 'Univ', 90000, [1, 4, 5], '#12345', 'blue');
let car2 = new Car('Mitsubishi', 'Outlander', 190, 'Univ', 90000, [1, 4, 5], 32345, 'blue');
console.log(car1.carInfo);
console.log(car1);
console.log(car1.getNameBrand);
console.log(car2);
let CarLexus = new Model('Lexus', 'RX350', 260);
//using setters
console.log(CarLexus);
CarLexus.setBrandModel = "Lexus RX250";
console.log(CarLexus);
CarLexus.setSpeed = '230';
console.log(CarLexus.getSpeed);
console.log(CarLexus.getNameBrand);
car1.setSpeed = -10;//"The speed cannot be negative!"
car1.setSpeed = 250;
car1.setCost = 100000;
//using getter
console.log(CarLexus.getBrandModelSpeed);
//methods 
console.log(car1.getPonts);
console.log(car1.getCost);
console.log(car1.numberOfOptions());
console.log(car1.bodyType);
