import { cityName,temperature, requestTime, description, icon } from './dom-elements';

export function fillHtmlNow(data: any): void {
  const now: string = new Date().toLocaleTimeString();
  const iconAdress: string = `http://openweathermap.org/img/wn/${data.weather[0].icon}@2x.png`;
  cityName.innerHTML = data.name;
  temperature.innerHTML = Math.round(data.main.temp - 273) + '&deg';
  requestTime.innerHTML = now;
  description.innerHTML = data.weather[0].description;
  icon.innerHTML = `<img src=${iconAdress}>`;
}

export function fillHtmlOneDay(hours: any): void {
  const cityNameText: string =  (document.getElementById('input') as HTMLInputElement).value;
  const now: string = new Date().toLocaleTimeString();

  cityName.innerHTML = cityNameText;
  requestTime.innerHTML = now;

  let time: number = +now.slice(0, 2);
  let iconAdress: string;
  let nameTrIconText: string;
  let nameTrTempText: string;
  let nameTrTimeText: string;
  let k: number = 0;
  for (let i: number = 6; i <= 24; i = i + 6) {

    nameTrIconText = `icon-tr_`;
    nameTrTempText = `temperature-tr_`;
    nameTrTimeText = `time-tr_`;
    iconAdress = `http://openweathermap.org/img/wn/${hours[i].weather[0].icon}@2x.png`;  

    nameTrIconText += k;
    nameTrTempText += k;
    nameTrTimeText += k;

    if ( time  + 6 < 24) {
      time = time + 6;
    } else if ( time + 6 === 24) {
      time = 0;
    } else if (time + 6 > 24) {
      time = time - 18;
    }
    const nameTrTime = <HTMLElement>document.getElementById(`${nameTrTimeText}`);
    const nameTrIcon = <HTMLElement>document.getElementById(`${nameTrIconText}`);
    const nameTrTemp = <HTMLElement>document.getElementById(`${nameTrTempText}`);
    nameTrTime.innerHTML = time + ':00';
    nameTrIcon.innerHTML = `<img src=${iconAdress}>`;
    nameTrTemp.innerHTML = Math.round(hours[i].temp - 273) + '&deg';
    k++;
  }
}

export function clearHtml(): void {
  cityName.innerHTML = '';
  temperature.innerHTML = '';
  requestTime.innerHTML = '';
  description.innerHTML = '';
  icon.innerHTML = '';
}