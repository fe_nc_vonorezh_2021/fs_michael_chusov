import { Component, OnInit } from '@angular/core';
import { Animal } from '../../../models/animal-model';
import { AppService } from '../../../service/app.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-animal-info',
  templateUrl: './animal-info.component.html',
  styleUrls: ['./animal-info.component.less']
})
export class AnimalInfoComponent implements OnInit {

  public animal: Animal = new Animal();

  constructor(
    private activateRoute: ActivatedRoute,
    private location: Location,
    private service: AppService
  ) { }

  public ngOnInit(): void {
    this.getAnimal();
  }

  private getAnimal(): void {
    const id = +this.activateRoute.snapshot.paramMap.get('id');
    this.service.getAnimalById(id)
      .subscribe(animal => {
        this.animal = animal;
      });
  }

  public goBack(): void {
    this.location.back();
  }
}
