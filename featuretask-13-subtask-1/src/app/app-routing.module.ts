import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AnimalListComponent } from './modules/animal/animal-list/animal-list.component'
import { AddAnimalComponent } from './modules/add-animal/add-animal.component';
import { AnimalInfoComponent } from './modules/animal/animal-info/animal-info.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { EditAnimalComponent } from './modules/edit-animal/edit-animal.component';
import { AppService } from './service/app.service';
import { HttpClient } from '@angular/common/http';
import { HttpClientModule } from "@angular/common/http";

const routes: Routes = [
  { path: '', redirectTo: 'animals', pathMatch: 'full' },
  { path: 'animals', component: AnimalListComponent },
  { path: 'add-animal', component: AddAnimalComponent },
  { path: 'animal-info/:id', component: AnimalInfoComponent },
  { path: 'edit-animal/:id', component: EditAnimalComponent }
];

@NgModule({
  declarations: [
    AddAnimalComponent
  ],
  imports: [
    RouterModule.forRoot(routes),
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    HttpClientModule
  ],
  exports: [
    RouterModule
  ],
  providers: [AppService, HttpClientModule, HttpClient]
})

export class AppRoutingModule { }
