export class Brand {
  nameBrand: string;
  constructor(nameBrand: string) {
    this.nameBrand = nameBrand;
  }

  get getNameBrand(): string {
    return this.nameBrand;
  }
}