import { Component } from '@angular/core';
import { AppService } from './service/app.service';
import { Animal } from './animal/animal';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [
    AppService
  ]
})
export class AppComponent {

  public _animalsList: Animal[] = this.service.getAnimalsList();
  public _catsIsVisible: boolean = true;

  constructor(private service: AppService) {}

  public _activateAnimal (id: number): void {
    this.service.getAnimalById(id).isActive = !this.service.getAnimalById(id).isActive;
  }

  public _isActive(id: number): boolean {
    return this.service.getAnimalById(id).isActive;
  }

  public _toggleVisibilityCats(): void {
    this._catsIsVisible = !this._catsIsVisible;
    if (this._catsIsVisible) {
      this._animalsList = this.service.getAnimalsList();
    } else {
      this._animalsList = this.service.getFilteredAnimalsList();
    }
  }
  
}


