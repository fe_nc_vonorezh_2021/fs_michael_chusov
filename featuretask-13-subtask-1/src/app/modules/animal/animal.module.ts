import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnimalListComponent } from './animal-list/animal-list.component';
import { AnimalInfoComponent } from './animal-info/animal-info.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from "@angular/common/http";
import { RouterTestingModule } from "@angular/router/testing";
import { AppService } from 'src/app/service/app.service';
import { HttpClient } from '@angular/common/http';

@NgModule({
  declarations: [
    AnimalListComponent,
    AnimalInfoComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterTestingModule
  ],
  exports: [
    AnimalListComponent
  ],
  providers: [HttpClientModule, HttpClient]
})
export class AnimalModule { }
