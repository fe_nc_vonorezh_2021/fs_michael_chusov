 const gulp = require('gulp');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');

gulp.task('js', function () {
  return gulp.src('./task_4/**/**.js')
    .pipe(concat('test.mini.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./dist/'))
});
