import { fillHtmlOneDay } from "./fill-html";

export async function requestWeatherOneDay(lat: number, lon: number) {
  const url: string = `https://api.openweathermap.org/data/2.5/onecall?` +
    `lat=${lat}&lon=${lon}&exclude={current,minutley,daily,alerts}&appid=87a4e3b57fd5877a435275e9fae66ccd`;
  const response = await fetch(url);
  if (response.ok) {
    const data = await response.json();
    fillHtmlOneDay(data.hourly);
  } else {
    alert('Wrong city');
    console.log("Wrong: " + response.status);
  }
}