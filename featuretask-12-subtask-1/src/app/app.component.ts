import { Component} from '@angular/core';

import { AppService } from './service/app.service';
import { Animal } from './animal/animal';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less'],
  providers: [
    AppService
  ],
})
export class AppComponent {

  constructor(private service: AppService) { }

  public animalsList: Animal[] = this.service.getAnimalsList();

  public _catsIsVisible: boolean = true;

  public _activateAnimal(id: number): void {
    this.service.getAnimalById(id).isActive = !this.service.getAnimalById(id).isActive;
  }

  public _isActive(id: number): boolean {
    return this.service.getAnimalById(id).isActive;
  }

  public _toggleVisibilityCats(): void {
    this._catsIsVisible = !this._catsIsVisible;
    this.animalsList = this._catsIsVisible ? this.service.getAnimalsList() : this.service.getFilteredAnimalsList();
  }

}
