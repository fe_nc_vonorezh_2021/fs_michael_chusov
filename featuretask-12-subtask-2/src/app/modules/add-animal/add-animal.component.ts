import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppService } from 'src/app/service/app.service';

@Component({
  selector: 'app-add-animal',
  templateUrl: './add-animal.component.html',
  styleUrls: ['./add-animal.component.less']
})

export class AddAnimalComponent implements OnInit {

  public formGroup: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private service: AppService,
  ) { }

  public ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      type: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(10)]],
      name: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(10)]],
      sex: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(1)]],
      weight: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(10)]],
      color: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(10)]]
    });
  }

  public onSubmit(): void {
    if (this.formGroup.valid) {
      this.service.postAnimal(this.formGroup.value)
        .subscribe(res => {
          alert("Animal added successfully");
        },
          err => {
            alert("Wrong");
          });
    } else {
      alert("Wrong");
    }
  }
}

