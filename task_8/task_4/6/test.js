function Click() {
  let number = Math.round(Math.random() * (1001 - 1) + 1);
  let a = prompt("Введите число: ");
  let count = 0;
  do {
    count++;
    if (a.replace(/\s/g, '').length === 0 || isNaN(a)) {
      a = prompt("Введите число");
    }
    else {
      if (a > number) {
        a = prompt("Искомое число меньше");
      }
      else {
        if (a < number) {
          a = prompt("Искомое число больше");
        }
        else {
          console.log(a);
          console.log(number);
          break;
        }
      }
    }
  } while (a !== number);
  if (confirm("Вы угадали! Количество попыток: " + count + ". Начать заново?")) {
    Click();
  }
  else {
    window.close();
  }
}
