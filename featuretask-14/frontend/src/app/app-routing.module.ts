import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AnimalListComponent } from './modules/components/animal-list/animal-list.component'
import { AddAnimalComponent } from './modules/components/add-animal/add-animal.component';
import { AnimalInfoComponent } from './modules/components/animal-info/animal-info.component'
import { EditAnimalComponent } from './modules/components/edit-animal/edit-animal.component';

const routes: Routes = [
  { path: '', redirectTo: 'animals', pathMatch: 'full' },
  { path: 'animals', component: AnimalListComponent },
  { path: 'add-animal', component: AddAnimalComponent },
  { path: 'animal-info/:id', component: AnimalInfoComponent },
  { path: 'edit-animal/:id', component: EditAnimalComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule { }
