import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AnimalModule } from './modules/animal/animal.module'

import { AppComponent } from './app.component';
import { HeaderComponent } from './header-component/header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
  ],
  imports: [
    BrowserModule, AnimalModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
