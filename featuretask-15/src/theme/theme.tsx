import { createContext, useMemo, useState } from "react";
import { useContext } from 'react'

// type TypeThemeContext = {
//   isDark: boolean;
// }

const ThemeContext = createContext({ darkTheme: false });

const useTheme = () => {
  const theme = useContext(ThemeContext)
  return theme
}

export const ThemeProvider: React.FC = ({ children }) => {
  
    const [darkTheme, setDarkTheme] = useState<boolean>(false);

    const value = useMemo<any>(() => ({ darkTheme, setDarkTheme }), [darkTheme]);

    return <>
        <ThemeContext.Provider value={value}>
            {children}
        </ThemeContext.Provider>
      </>
        
    
}

export default useTheme;
