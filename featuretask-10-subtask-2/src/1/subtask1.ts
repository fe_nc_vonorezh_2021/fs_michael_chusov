import { range, filter } from "rxjs";
import { Observable } from "rxjs";

function isSimple(value: number): boolean {
  let flagSimple: boolean = true;
  for (let i = 2; i < value; i++) {
    if (value % i == 0) {
      flagSimple = false;
      break;
    }
  }
  return flagSimple;
}

const stream$: Observable<number> = range(1, 100)
  .pipe(
    filter((value: number) => isSimple(value))
  );
stream$.subscribe(
  (value: number) => { console.log(value) }
);

