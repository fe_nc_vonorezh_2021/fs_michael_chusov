import { Animal } from '../animal/animal';
import { FC } from 'react';

type TypeAnimalInfo = {
  showInfo: boolean;
  animal: Animal;
}

const AnimalInfo: FC<TypeAnimalInfo> = ({ showInfo, animal }) => {
  if(showInfo)
    return (<>
      <p>Sex: { animal.sex }</p>
      <p>Weight: { animal.weight }</p>
      <p>Color: { animal.color }</p>
      </>
    )
  return <>
  </>
}

export default AnimalInfo;