|Date       |  Name          | Discription  |Link to MR|
| ------------- |:-------------:| :-----:|-----|
|Oct 13 2021|  Task 1 | README.md, changelog.md |https://gitlab.com/fe_nc_vonorezh_2021/fs_michael_chusov/-/merge_requests/2|
|Oct 21 2021| Task 2 | html, css |https://gitlab.com/fe_nc_vonorezh_2021/fs_michael_chusov/-/merge_requests/3| 
|Oct 24 2021|Task 4| 7 tasks (JS) |https://gitlab.com/fe_nc_vonorezh_2021/fs_michael_chusov/-/merge_requests/4|
|Nov 03 2021|Task 5.1|calculator |https://gitlab.com/fe_nc_vonorezh_2021/fs_michael_chusov/-/merge_requests/5|
|Nov 04 2021|Task 5.2| JS and OOP|https://gitlab.com/fe_nc_vonorezh_2021/fs_michael_chusov/-/merge_requests/6|
|Nov 07 2021|Task 6|modal form|https://gitlab.com/fe_nc_vonorezh_2021/fs_michael_chusov/-/merge_requests/7|
|Nov 09 2021|Task 7|interactive table|https://gitlab.com/fe_nc_vonorezh_2021/fs_michael_chusov/-/merge_requests/8|
|Nov 15 2021|Task 8|gulp and Webpack|https://gitlab.com/fe_nc_vonorezh_2021/fs_michael_chusov/-/merge_requests/10|
|Nov 18 2021|Task 9|typescript OOP|https://gitlab.com/fe_nc_vonorezh_2021/fs_michael_chusov/-/merge_requests/11|
|Nov 24 2021|Task 10.1|doubly linked list, quick sort|https://gitlab.com/fe_nc_vonorezh_2021/fs_michael_chusov/-/merge_requests/13|
|Nov 24 2021|Task 10.2|RxJS|https://gitlab.com/fe_nc_vonorezh_2021/fs_michael_chusov/-/merge_requests/12|
