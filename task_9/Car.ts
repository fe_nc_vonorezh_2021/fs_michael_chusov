import { Equipment } from "./Equipment";
import { StrNumb } from "./StrNumb";

export class Car extends Equipment {
  private id: StrNumb;
  color: string;
  constructor(nameBrand: string, nameModel: string, maxSpeed: StrNumb, bodyType: string, cost: StrNumb, options: number[], id: StrNumb, color: string) {
    super(nameBrand, nameModel, maxSpeed, bodyType, cost, options);
    this.id = id;
    this.color = color;
  }

  set setColor(color: string): void {
    this.color = color;
  }

  get carInfo(): string {
    return 'CarInfo: \n' + this.nameBrand + ' ' + this.nameModel + '\n' + this.cost + '\n' + this.color;
  }
}