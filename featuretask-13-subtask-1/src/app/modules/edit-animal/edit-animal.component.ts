import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Animal } from 'src/app/animal-class/animal';
import { AppService } from 'src/app/service/app.service';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-animal',
  templateUrl: './edit-animal.component.html',
  styleUrls: ['./edit-animal.component.less']
})

export class EditAnimalComponent implements OnInit {

  animal: Animal = new Animal();

  constructor(
    private service: AppService,
    private location: Location,
    private activeRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.getAnimal();
  }

  onSubmit(form: NgForm): void {
    if (form.valid) {
      this.service.updateAnimal(this.animal, this.animal.id)
        .subscribe(res => {
          alert("Animal edited successfully");
        },
          err => {
            alert("Wrong");
          });
      this.goBack();
    } else {
      alert("Wrong");
    }

  }

  getAnimal(): void {
    const id = +this.activeRoute.snapshot.paramMap.get('id');
    this.service.getAnimalById(id)
      .subscribe(animal => {
        this.animal = animal;
      });
  }

  goBack(): void {
    this.location.back();
  }
}
