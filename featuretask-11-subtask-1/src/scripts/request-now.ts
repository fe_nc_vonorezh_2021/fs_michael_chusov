import { clearHtml, fillHtmlNow } from "./fill-html";
import { input } from "./dom-elements";

export async function requestWeatherNow() {
  const cityName: string = input.value;
  const url: string = `http://api.openweathermap.org/data/2.5/weather?q=${cityName}&appid=87a4e3b57fd5877a435275e9fae66ccd`;
  clearHtml();
  const response = await fetch(url);
  if (response.ok) {
    const data= await response.json();
    fillHtmlNow(data);
  } else {
    alert('Wrong city');
    console.log("Wrong: " + response.status);
  }
}