const path = require('path')

module.exports = {
  context: path.resolve(__dirname, 'src'),
  entry: {
    subtask1: './1/subtask1.js',
    subtask2: './2/subtask2.js',
    subtask3: './3/subtask3.js'
  },

  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist')
  }
};
