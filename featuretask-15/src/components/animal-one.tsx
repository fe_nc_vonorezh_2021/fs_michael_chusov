import { Animal } from '../animal/animal';
import { FC } from 'react';
import { useState } from 'react'
import AnimalInfo from './animal-info'

type OneAnimalType = {
  animal: Animal;
}

const OneAnimal: FC<OneAnimalType> = ({ animal }) => {
  const [showInfo, setShowInfo] = useState<boolean>(false);
  
  const changeActive = () => {
    setShowInfo(prev => !prev)
  }

  return <>
    <li
      onClick={ changeActive }
    >
      { animal.type } { animal.name }
      <AnimalInfo
        showInfo={ showInfo }
        animal={ animal }
      />
    </li>
  </>
}

export default OneAnimal;
