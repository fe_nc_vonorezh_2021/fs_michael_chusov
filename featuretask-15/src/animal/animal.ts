export class Animal {
  id: number;
  type: string;
  name: string;
  sex: string;
  weight: number;
  color: string;

  constructor(id: number, type: string, name: string, sex: string, weight: number, color: string) {
    this.id = id;
    this.type = type;
    this.name = name;
    this.sex = sex;
    this.weight = weight;
    this.color = color;
  }

}
