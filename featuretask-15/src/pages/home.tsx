import AnimalList from '../components/animal-list'
import useTheme from '../theme/theme'
import Header from '../components/header'

const HomePage = () => {
  const { darkTheme, setDarkTheme }: any = useTheme()

  const changeTheme = () => {
    setDarkTheme(darkTheme => !darkTheme)
  }

  return (<div className={darkTheme ? 'dark' : 'light'}>
      <Header headText={'Pet search application'} darkTheme={darkTheme} changeTheme={changeTheme} />
      <AnimalList/>
    </div>
  )
}

export default HomePage;