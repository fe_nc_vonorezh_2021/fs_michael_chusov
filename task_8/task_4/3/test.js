function insertionSort(arr, type) {
    for (let i = 1, n = arr.length; i < n; i++) {
      const current = arr[i];
      let j = i;
      if (type) {
        while (j > 0 && arr[j - 1] <= current) {
          arr[j] = arr[j - 1];
          j--;
        }
      }
      else {
        while (j > 0 && arr[j - 1] > current) {
          arr[j] = arr[j - 1];
          j--;
        }
      }
      arr[j] = current;
    }
  return arr;
}
function sumSquare(arr) {
  for (let n = arr.length, i = 0; i < n; i++){
    if (arr[i] % 2 !== 0){
      sum += Math.pow(arr[i], 2);
    } 
  } 
  return sum;
}

let arr = [];
let sum = 0;
for (let i = 0, n = 40; i < n; i++) {
  arr.push(Math.round(Math.random() * n));
}
console.log("Начальный массив: " + arr);
console.log("По убыванию: " + insertionSort(arr, true));
console.log("По возрастанию: " + insertionSort(arr, false));
console.log("Сумма квадратов нечетных чисел: " + sumSquare(arr));
