export const input = <HTMLInputElement>document.getElementById('input');
export const temperature = <HTMLElement>document.querySelector('.temperature');
export const description = <HTMLElement>document.querySelector('.description');
export const icon = <HTMLElement>document.getElementById('icon');
export const featuresOneDay = <HTMLElement>document.getElementById('features-oneDay');
export const cityName = <HTMLElement>document.getElementById('cityName');
export const requestTime = <HTMLElement>document.querySelector('requestTime');

export const buttonShowNow: any = document.getElementById('button-showNow');
export const buttonShowOneDay: any = document.getElementById('button-showOneDay');