import { AppService } from '../service/service';
import { Animal } from '../animal/animal';
import { FC, useEffect } from 'react';
import { useState } from 'react'
import ButtonVisCats from '../components/button-vis-cats'
import OneAnimal from './animal-one';

const animalList = new AppService;
const list = animalList.getAnimalsList();

const AnimalList: React.FC = () => {

  const [animals, setAnimals] = useState<Animal[]>([]);
  const [withCats, setWithCats] = useState<boolean>(false);

  useEffect(() => {
    setAnimals(list);
  }, [])

  const hideCats = () => {
      withCats
        ? setAnimals(list)
        : setAnimals (animals => animals.filter(animal => animal.type !== 'Cat'))
    setWithCats(withCats => !withCats)
  }

  return (<>
      <ul>
        {animals.map((animal) =>
            <OneAnimal key={animal.id.toString()}
              animal={ animal }
            />
          )
        }
      </ul>
      <ButtonVisCats hideCats={ hideCats } withCats={ withCats }/>
    </>
  )
}

export default AnimalList;