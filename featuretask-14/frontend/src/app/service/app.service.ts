import { Injectable } from '@angular/core';
import { Animal } from '../models/animal-model';
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators"
import { Observable } from 'rxjs';

@Injectable()
export class AppService {

  constructor(
    private http: HttpClient
  ) { }

  public postAnimal(data: Animal): Observable<Animal> {
    return this.http.post<Animal>("http://localhost:3000/animals", data)
      .pipe(map((res: Animal) => {
        return res;
      }));
  }

  public getAnimal(): Observable<Animal[]> {
    return this.http.get<Animal[]>("http://localhost:3000/animals")
      .pipe(map((res: Animal[]) => {
        return res;
      }));
  }

  public getAnimalById(id: number): Observable<Animal> {
    return this.http.get<Animal>(`http://localhost:3000/animals/${id}`)
      .pipe(map((res: Animal) => {
        return res;
      }));
  }

  public updateAnimal(data: Animal, id: number): Observable<Animal> {
    return this.http.put<Animal>("http://localhost:3000/animals/" + id, data)
      .pipe(map((res: Animal) => {
        return res;
      }));
  }

  public deleteAnimal(id: number): Observable<Animal> {
    return this.http.delete<Animal>("http://localhost:3000/animals/" + id)
      .pipe(map((res: Animal) => {
        return res;
      }));
  }
}
