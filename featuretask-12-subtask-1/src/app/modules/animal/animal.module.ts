import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnimalListComponent } from './animal-list/animal-list.component';
import { AnimalInfoComponent } from './animal-info/animal-info.component';

@NgModule({
  declarations: [
    AnimalListComponent,
    AnimalInfoComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    AnimalListComponent
  ]
})
export class AnimalModule { }
