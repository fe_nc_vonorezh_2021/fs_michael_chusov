type ButtonVCProps = {
  hideCats: () => void;
  withCats: boolean;
}

const ButtonVisCats = (props: ButtonVCProps) => {
  return <button
    type='button'
    id="buttonVisCats"
    onClick={props.hideCats}
  >
    { !props.withCats ? 'Hide Cats' : 'Show Cats' }
  </button>
}

export default ButtonVisCats;