export class Animal {
  public id: number;
  public type: string;
  public name: string;
  public sex: string;
  public weight: number;
  public color: string;
  public isActive: boolean = false;

  constructor(id: number, type: string, name: string, sex: string, weight: number, color: string) {
    this.id = id;
    this.type = type;
    this.name = name;
    this.sex = sex;
    this.weight = weight;
    this.color = color;
  }
  
}

