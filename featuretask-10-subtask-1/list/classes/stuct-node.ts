export class Node {
  private data: any;
  public prev: Node | null = null;
  public next: Node | null = null;

  constructor(data?: any) {
    this.data = data;
  }
}