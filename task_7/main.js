//Суть в том, что при клике на любую td, в ней создается input.
//При повторном клике редактируем input.
//При потере фокуса записываем в td значение, которое было введено в input
//И удаляем input
const updateTable = () => {
  //при первом запуске k = 0 (чтобы пройти по всему массиву)
  //при дальнейших запусках идем с k-го элемента до конца нового массива(чтобы пройти 7 новых td)
  let k = tdAll.length;
  //Каждый раз получаем заново весь список td.
  tdAll = document.querySelectorAll('td:not(.del)');
  //В цикле при первом запуске пробегаем по всем td и вешаем на них addEventListener.
  //При дальнейших запусках функции (при добавлении новой строки) 
  //берем только последние 7 td и вешаем на них addEventListener.
  for (let i = k; i < tdAll.length; i++) {
    tdAll[i].addEventListener('click', function addInput() {
      //создаем новый инпут для конкретной ячейки
      let input = document.createElement('input');
      //меняем цвет ячейки
      this.style.background = 'rgb(199, 196, 196)';
      //перед появлением инпута в ячейке, передаем в него текст этой td 
      input.value = this.innerHTML;
      //очищаем td, чтобы при появлении инпута в ней не было текста
      this.innerHTML = '';
      //наконец, вставляем инпут, в который выше передали значение, в уже очищенную выше ячейку
      this.appendChild(input);
      //объявим константу td, в которую передадим this (чтобы во вложенной функции обращаться к this из главной функции)
      const td = this;
      //удалим Listener c нашей ячейке, так как мы на нее уже кликнули и она в фокусе
      this.removeEventListener('click', addInput);
      //Описываем функцию, которая запустится при потери фокуса с инпута.
      input.addEventListener('blur', function () {
        //цвет ячейк будет каким был до клика
        tdAll[i].style.background = '';
        //передадим значение из инпута в td
        td.innerHTML = this.value;
        //вернем addEventListener на td
        td.addEventListener('click', addInput);
      });
    });
  }
}

function deleteRow(row) {
  if(table.rows.length > 2){
  const i = row.parentNode.parentNode.rowIndex;
  table.deleteRow(i);
  } else {
    alert("Таблица останется пустой!");
  }
}

let tdAll = [];
const table = document.querySelector("table");
const addLineButton = document.getElementById("add-line");

addLineButton.addEventListener("click", () => {
  const newRow = table.rows[1].cloneNode(true);
  for (let i = 0; i < 7; i++) {
    newRow.cells[i].innerHTML = "";
  }
  table.appendChild(newRow);
  updateTable();
});

updateTable();
