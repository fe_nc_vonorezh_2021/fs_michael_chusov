import { FC } from 'react'

type TypeHeaderText = {
  headText: string;
  darkTheme: boolean;
  changeTheme: () => void;
}

const Header: FC<TypeHeaderText> = ({ headText, darkTheme, changeTheme }) => {
  return (<>
    <p style={{ textAlign: 'center', fontSize: 50 }} >
      {headText}
    </p>
    <button
      type='button'
      id="buttonChangeTheme"
      onClick={ changeTheme }
    >
      { darkTheme ? 'Dark' : 'light' }
    </button>
  </>
  )
}

export default Header;