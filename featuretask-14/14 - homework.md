# Netcracker Frontend School

## Домашнее задание к лекции на тему "Nginx & Docker & Express & MongoDB "
### Общие требования
* Код необходимо разместить в ветке Task_14.
* После выполнения задания завести MR и отправить куратору.

### Задание
* Написать back-end часть с ипользованием express и mongodb
* Изменить приложение про животных из предыдущих заданий для работы новым BE сервером. Собрать фронтенд  локально. (ng build -c production) 
* Настроить nginx для отображения вашего фронтенд приложения. 
* Завернуть nginx с приложением + express + mongodb в docker
* Добавить docker compose для запуска


### Список источников
1. [Express - Установка](https://expressjs.com/ru/starter/installing.html)
2. [Express - Пример “Hello world”](https://expressjs.com/ru/starter/hello-world.html)
3. [Express - Предоставление статических файлов в Express](https://expressjs.com/ru/starter/static-files.html)
4. [MongoDB - Install MongoDB Community Edition on Windows](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows) 
5. [Express - MongoDB](https://expressjs.com/ru/guide/database-integration.html#mongo)
6. [Building a simple app using NodeJS, MongoDB, and ExpressJS](https://medium.com/@thepradeep001/building-a-simple-app-using-nodejs-mongodb-and-expressjs-9678277e87e0)
7. [MongoDB - insertOne()](https://docs.mongodb.com/stitch/mongodb/actions/collection.insertOne)
8. [NGINX](https://nginx.org/ru)
9. [NGINX - Руководство для начинающих](https://nginx.org/ru/docs/beginners_guide.html)
10. [NGINX - Ускоряем Nginx за 5 минут](https://habr.com/ru/post/198982)
11. [NGINX - Модуль ngx_http_gzip_module](https://nginx.org/ru/docs/http/ngx_http_gzip_module.html) 
12. [Docker - Изучаем Docker]https://habr.com/ru/company/ruvds/blog/438796
13. [Docker - Docker Compose](https://docs.docker.com/compose)
14. [Docker - Get started with Docker Compose](https://docs.docker.com/compose/gettingstarted)
15. [Docker - Cleanup Commands](https://www.calazan.com/docker-cleanup-commands)
16. [How To Use the Official NGINX Docker Image](https://www.docker.com/blog/tips-for-deploying-nginx-official-image-with-docker)
17. [Используем официальный docker-образ NGINX](https://habr.com/ru/company/infobox/blog/265231)
18. [Docker - Dockerfile](https://infoboxcloud.ru/community/blog/infoboxcloud/200.html)
19. [Youtube - Docker Compose in 12 Minutes](https://www.youtube.com/watch?v=Qw9zlE3t8Ko)
20. [Youtube - Kubernetes vs Docker](https://www.youtube.com/watch?v=2vMEQ5zs1ko)
