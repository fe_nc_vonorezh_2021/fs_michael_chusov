export class Animal {
  public id: number;
  public type: string;
  public name: string;
  public sex: string;
  public weight: number;
  public color: string;
}
