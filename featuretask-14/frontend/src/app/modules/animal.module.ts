import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnimalListComponent } from './components/animal-list/animal-list.component';
import { AnimalInfoComponent } from './components/animal-info/animal-info.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddAnimalComponent } from './components/add-animal/add-animal.component';
import { EditAnimalComponent } from './components/edit-animal/edit-animal.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    AnimalListComponent,
    AddAnimalComponent,
    EditAnimalComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule
  ]
})

export class AnimalModule { }
