import { Component, OnInit, ComponentFactoryResolver } from '@angular/core';
import { AppService } from '../../../service/app.service';
import { Animal } from '../../../models/animal-model';
import { ViewChild, ViewContainerRef, ComponentFactory, ComponentRef } from "@angular/core";
import { AddAnimalComponent } from '../add-animal/add-animal.component';

@Component({
  selector: 'app-animal-list',
  templateUrl: './animal-list.component.html',
  styleUrls: ['./animal-list.component.less'],
})

export class AnimalListComponent implements OnInit {

  @ViewChild("formContainer", { read: ViewContainerRef }) container: any;
  public componentRef: ComponentRef<any>;
  public isModalShow: boolean = false;

  public animals: Animal[];
  public _catsIsVisible: boolean = true;

  constructor(
    private service: AppService,
    private resolver: ComponentFactoryResolver
  ) { }

  public ngOnInit(): void {
    this.getAnimals();
  }

  private getAnimals(): void {
    this.service.getAnimal()
      .subscribe(res => {
        this.animals = res;
      });
  }

  public deleteAnimal(animal: Animal): void {
    this.service.deleteAnimal(animal.id)
      .subscribe(res => {
        this.getAnimals();
      });
  }

  private getFilteredAnimals(): void {
    this.animals = this.animals.filter(animal => animal.type !== "Cat");
  }

  public _toggleVisibilityCats(): void {
    this._catsIsVisible = !this._catsIsVisible;
    this._catsIsVisible
      ? this.getAnimals()
      : this.getFilteredAnimals()
  }

  public closeModal(): void {
    this.isModalShow = false;
    this.container.clear();
  }

  public createComponent(): void {
    this.isModalShow = true;
    this.container.clear();
    const factory: ComponentFactory<AddAnimalComponent> = this.resolver.resolveComponentFactory(AddAnimalComponent);
    this.componentRef = this.container.createComponent(factory);
  }
}
