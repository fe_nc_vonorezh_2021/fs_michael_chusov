import { Selector } from "testcafe";

fixture("AppTest").page("http://localhost:4200/animals");

test("hide-cats-test", async (t) => {
  await t
    .click("#button-visibilityCats")
    .click("#button-visibilityCats");
});

test("add-animal-test", async (t) => {
  await t
    .click("#add-button")
    .typeText("#type", "Dog")
    .typeText("#name", "Anny")
    .typeText("#sex", "F")
    .typeText("#weight", "2")
    .typeText("#color", "Red")
    .setNativeDialogHandler((alert) => true)
    .click("#submit-button")
    .click("#close-add-form-button");
});

test("delete-animal-test", async (t) => {
  const deleteButton = Selector("li").find("#delete");
  await t
    .click(deleteButton);
});

test("edit-animal-test", async (t) => {
  const editButton = Selector("li").find("#edit-button");
  await t
    .click(editButton)
    .click("#weight")
    .pressKey("ctrl+a delete")
    .typeText("#weight", "1")
    .click("#type")
    .pressKey("ctrl+a delete")
    .typeText("#type", "Test_type")
    .setNativeDialogHandler((alert) => true)
    .click("#submit-button");
});