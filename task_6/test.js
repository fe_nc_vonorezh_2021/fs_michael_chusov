function openForm() {
  document.getElementById("myForm").style.display = "block";
}

function closeForm() {
  document.getElementById("myForm").style.display = "none";
}

alertBtnSend.onclick = function () {
  document.getElementById("myCustomAlert").style.display = "none";
}

function customAlert (str) {
  document.getElementById("myCustomAlert").style.display = "block";
  document.getElementById("output").value = str;
}
//Нажатие на кнопку отправить
let clickSendVisual = () => {
  document.getElementById('btnSend').style.background = '#f1f1f1'
  document.getElementById('btnSend').style.color = '#AF9559'
  setTimeout(() => document.getElementById('btnSend').style.background = '', 40);
  setTimeout(() => document.getElementById('btnSend').style.color = '', 40);
}
//Нажатие на кнопку Войти (на шапке)
let clickLoginVisual = () => {
  document.getElementById('login').style.color = 'white'
  setTimeout(() => document.getElementById('login').style.color = '', 50);
}

function clickSend() {
  localStorage.removeItem('firstName');
  localStorage.removeItem('lastName');
  localStorage.removeItem('email');
  localStorage.removeItem('phoneNumber');
  localStorage.removeItem('message');
  
  if (flag) {
    if (getCookie('flag')) {
      closeForm();
      customAlert(formInfo.firstName + " " + formInfo.lastName + ", ваше обращение обрабатывается!");
    } else {
      closeForm();
      customAlert(formInfo.firstName + " " + formInfo.lastName + ", спасибо за обращение!");
      document.cookie = 'flag = true', 'firstName = ${formInfo.firstName}', 'lastName = ${formInfo.lastName}';
    }
  } 
}

function add_firstName() {
  formInfo.firstName = document.querySelector("input[name='firstName']").value;
  localStorage.setItem('firstName', formInfo.firstName);
}

function add_lastName() {
  formInfo.lastName = document.querySelector("input[name='lastName']").value;
  localStorage.setItem('lastName', formInfo.lastName);
}

function add_email() {
  formInfo.email = document.querySelector("input[name='email']").value;
  localStorage.setItem('email', formInfo.email);
}

function add_phoneNumber() {
  formInfo.phoneNumber = document.querySelector("input[name='phoneNumber']").value;
  localStorage.setItem('phoneNumber', formInfo.phoneNumber);
}

function add_message() {
  formInfo.message = document.querySelector("textarea[name='fildMessage']").value;
  localStorage.setItem('message', formInfo.message);
}

function whiteSpace(str) {
  return /^\s*$/.test(str);
}

function correctEmail(str) {
  return /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/.test(str);
}

function correctPhoneNumber(str) {
  return /^\+\d{1}\(\d{3}\)\d{2}-\d{2}-\d{3}$/.test(str);
}

function getCookie(name) {
  let matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

let timerId = setInterval(() => {
  if (whiteSpace(formInfo.firstName) || formInfo.firstName === null) {
    document.querySelector("input[name='firstName']").style.background = "#979292";
    flag1 = false;
  } else {
    document.querySelector("input[name='firstName']").style.background = "";
    flag1 = true;
  }
  if (whiteSpace(formInfo.lastName) || formInfo.lastName === null) {
    document.querySelector("input[name='lastName']").style.background = "#979292";
    flag2 = false;
  } else {
    document.querySelector("input[name='lastName']").style.background = "";
    flag2 = true;
  }
  if (!correctEmail(formInfo.email) || !correctEmail(formInfo.email)) {
    document.querySelector("input[name='email']").style.background = "#979292";
    flag3 = false;
  } else {
    document.querySelector("input[name='email']").style.background = "";
    flag3 = true;
  }
  if (!correctPhoneNumber(formInfo.phoneNumber)) {
    if (!whiteSpace(formInfo.phoneNumber) && formInfo.phoneNumber !== null) {
      document.querySelector("input[name='phoneNumber']").style.background = "#979292";
      flag4 = false;
    } else {
      document.querySelector("input[name='phoneNumber']").style.background = "";
      flag4 = true;
    } 
  } else {
    document.querySelector("input[name='phoneNumber']").style.background = "";
    flag4 = true;
  }
  if (whiteSpace(formInfo.message) || formInfo.message === null ) {
    document.querySelector("textarea[name='fildMessage']").style.background = "#979292";
    flag5 = false;
  } else {
    document.querySelector("textarea[name='fildMessage']").style.background = "";
    flag5 = true;
  }
  if (!(flag1 && flag2 && flag3 && flag4 && flag5)) {
    flag = false;
    document.getElementById('btnSend').style.background = "#979292";
    document.getElementById('btnSend').setAttribute('disabled', true);
    document.getElementById('btnSend').style.cursor = "auto";
  } else {
    flag = true;
    document.getElementById('btnSend').style.background = "";
    document.getElementById('btnSend').removeAttribute('disabled', true);
    document.getElementById('btnSend').style.cursor = "";
    document.getElementById('btnSend').style.color = "black";
  }
}, 100);

let flag = true;
let flag1 = false;
let flag2 = false;
let flag3 = false;
let flag4 = false;
let flag5 = false;

let formInfo = {
  firstName: '',
  lastName: '',
  email: '',
  phoneNumber: '',
  message: ''
};

formInfo.firstName = localStorage.getItem('firstName');
formInfo.lastName = localStorage.getItem('lastName');
formInfo.email = localStorage.getItem('email');
formInfo.phoneNumber = localStorage.getItem('phoneNumber');
formInfo.message = localStorage.getItem('message');

document.querySelector("input[name='firstName']").value = localStorage.getItem('firstName');
document.querySelector("input[name='lastName']").value = localStorage.getItem('lastName');
document.querySelector("input[name='email']").value = localStorage.getItem('email');
document.querySelector("input[name='phoneNumber']").value = localStorage.getItem('phoneNumber');
document.querySelector("textarea[name='fildMessage']").value = localStorage.getItem('message');
