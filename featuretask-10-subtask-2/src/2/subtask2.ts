import { Observable, map, range, filter } from 'rxjs';

const stream$: Observable<number> = range(1, 6);
stream$.pipe(
  map(
    function (value: number): any {
      try {
        if (value > 5) {
          throw new Error();      
        } else {
          return (-1) * value + 6;
        }
      } catch (error) {
        console.log(error);
      }
    }
  ),
  filter((val: number) => val !== undefined)
)
  .subscribe(
    (val: number) => { console.log(val) },
    (err: string) => { console.log(err) },
    () => { console.log('complete') }
  );
