import { Model } from "./Model";

export class BmwFactory {
  create(type: string): any {
    if (type == 'X5') {
      return new Model('Bmw', type, 250);
    }
    if (type == 'X6') {
      return new Model('Bmw', type, 280);
    }
    if (type == 'M5') {
      return new Model('Bmw', type, 300);
    }
  }
}