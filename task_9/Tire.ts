export interface Tire {
  radius: number,
  size: {
    first: number,
    second: number
  }
}