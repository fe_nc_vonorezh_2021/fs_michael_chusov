import { featuresOneDay, temperature, description, icon } from './dom-elements';
import { requestWeatherNow } from './request-now';
import { requestWeatherOneDay } from './request-one-day';
import { buttonShowNow, buttonShowOneDay } from './dom-elements';
import { requestCoord } from './request-cord';

buttonShowNow.addEventListener('click', () => {
  featuresOneDay.style.display = "none";
  temperature.style.display = "block";
  description.style.display = "block";
  icon.style.display = "block";
  requestWeatherNow();
});

buttonShowOneDay.addEventListener('click', () => {
  temperature.style.display = "none";
  description.style.display = "none";
  icon.style.display = "none";
  featuresOneDay.style.display = "block";
  const coords = requestCoord();
  coords.then(result => {
    requestWeatherOneDay(result.lat, result.lon);
  });
});
