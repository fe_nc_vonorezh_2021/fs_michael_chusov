import { Tire } from "./Tire";

export interface TireSize extends Tire {
  getTireSize: () => string;
}