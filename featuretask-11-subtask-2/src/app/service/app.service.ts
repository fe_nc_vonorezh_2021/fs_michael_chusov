import { Injectable } from '@angular/core';
import { Animal } from '../animal/animal';

@Injectable()
export class AppService {
  private animalsList: Animal[] = [
    new Animal(0, 'Cat', 'Busya', 'F', 0.8, 'Gray'),
    new Animal(1, 'Dog', 'Lesya', 'F', 17, 'Creme brulee'),
    new Animal(2, 'Cat', 'Sonya', 'F', 3.1, 'Brown'),
    new Animal(3, 'Guinea pig', 'Loli', 'F', 0.6, 'White-gray'),
    new Animal(4, 'Rat', 'Smoke', 'M', 0.7, 'White'),
    new Animal(5, 'Turtle', 'Torpedo', 'F', 0.5, 'Green')
  ];

  getAnimalsList(): Animal[] {
    return this.animalsList;
  }

  getFilteredAnimalsList(): Animal[] {
    return this.animalsList.filter(animal => animal.type !== 'Cat');
  }

  getAnimalById(id: number): any {
    for (let i = 0; i < this.animalsList.length; i++) {
      if (this.animalsList[i].id === id) {
        return this.animalsList[i];
      }
    }
  }
}  
