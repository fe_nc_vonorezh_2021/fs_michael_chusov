import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditAnimalComponent } from './edit-animal.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppService } from 'src/app/service/app.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    EditAnimalComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [AppService, HttpClientModule]
})

export class EditAnimalModule { }
