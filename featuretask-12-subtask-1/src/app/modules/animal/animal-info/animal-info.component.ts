import { Component, Input, OnInit, Output, EventEmitter, HostBinding } from '@angular/core';
import { Animal } from '../../../animal/animal';

@Component({
  selector: 'app-animal-info',
  templateUrl: './animal-info.component.html',
  styleUrls: ['./animal-info.component.less']
})
export class AnimalInfoComponent implements OnInit {

  @Input()
  public animal: Animal;

  @Input()
  @HostBinding("class._active")
  public active: boolean = false;

  @Output()
  private onAnimalSelect: EventEmitter<number> = new EventEmitter();

  public ngOnInit(): void {
    console.log("ngOnInit", this.animal.name);
  }

  public _selectAnimal(): void {
    this.onAnimalSelect.emit(this.animal.id);
    this.animal.isActive = !this.animal.isActive;
  }

  public _isActive(): boolean {
    return this.animal.isActive;
  }

}
