import { Model } from "./Model";
import { StrNumb } from "./StrNumb";

export class Equipment extends Model {
  bodyType: string;
  cost: StrNumb;
  options: number[];
  constructor(nameBrand: string, nameModel: string, maxSpeed: StrNumb, bodyType: string, cost: StrNumb, options: number[]) {
    super(nameBrand, nameModel, maxSpeed);
    this.bodyType = bodyType;
    this.cost = cost;
    this.options = options;
  }

  set setBodyType(bodyType: string): void {
    this.bodyType = bodyType;
  }

  set setCost(cost: StrNumb): void {
    this.cost = cost;
  }

  set setOptoins(options: number[]): void {
    this.options = options;
  }

  get getPonts(): StrNumb {
    if (this.cost > 30000) {
      return this.cost;
    } else {
      return ':[';
    }
  }

  get getCost(): string {
    return 'Cost = ' + this.cost;
  }

  numberOfOptions(): string {
    return "Количество выбранных опций = " + this.options.length;
  }
}