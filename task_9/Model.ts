import { Brand } from "./Brand";
import { StrNumb } from "./StrNumb";

export class Model extends Brand {
  nameModel: string;
  maxSpeed: StrNumb;
  constructor(nameBrand: string, nameModel: string, maxSpeed: StrNumb) {
    super(nameBrand);
    this.nameModel = nameModel;
    this.maxSpeed = maxSpeed;
  }

  set setModel(model: string): void {
    this.nameModel = model;
  }

  set setSpeed(speed: StrNumb): void {
    if ((<number>speed) < 0) {
      console.log("The speed cannot be negative!");
    }
    this.maxSpeed = speed;
  }

  set setBrandModel(str: string): void {
    [this.nameBrand, this.nameModel] = str.split(" ");
  }
  get getBrandModelSpeed(): string {
    return "Brand: " + this.nameBrand + "\n" + "Model: " + this.nameModel + "\n" + "Speed: " + this.maxSpeed;
  }

  get getSpeed(): string {
    return 'Максимальная скорость = ' + this.maxSpeed;
  }
}
