import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddAnimalComponent } from './add-animal.component';

@NgModule({
  declarations: [
    AddAnimalComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule
  ]
})

export class AddAnimalModule { }
