import { Node } from "./stuct-node";

export class List {
  private head: Node | null = null;
  private tail: Node | null = null;
  private length: number = 0;

  public getElement = (index: number): any => {
    if (index < 0 || index >= this.length) {
      console.log("Неверное значение индекса");
    } else {
      let previous: Node | null = this.tail;
      let current: Node | null = this.head;
      if (index < this.length / 2) {
        let i: number = 0;
        while (i++ < index) {
          current = current.next;
        }
        return current.data;
      } else {
        let i: number = this.length - 1;
        while (i-- > index) {
          previous = previous.prev;
        }
        return previous.data;
      }
    }
  };

  public getAll = (): any[] => {
    let arr: any[] = [];
    for (let index: number = 0; index < this.length; index++) {
      arr.push(this.getElement(index));
    }
    return arr;
  };

  public append = <T>(data: T): void => {
    let newNode: Node = new Node(data);
    if (this.length === 0) {
      this.head = newNode;
      this.tail = newNode;
    } else {
      if (this.tail !== null) {
        this.tail.next = newNode;
        newNode.prev = this.tail;
        this.tail = newNode;
      }
    }
    this.length += 1;
  };

  public insert = <T>(index: number, data: T): void => {
    if (index < 0 || index > this.length) {
      console.log("Неверное значени индекса");
    } else {
      let newNode: Node = new Node(data);
      if (this.length == 0) {
        this.head = newNode;
        this.tail = newNode;
      } else {
        if (index === 0) {
          this.head.prev = newNode;
          newNode.next = this.head;
          this.head = newNode;
        } else if (index === this.length) {
          newNode.prev = this.tail;
          this.tail.next = newNode;
          this.tail = newNode;
        } else {
          let current = this.head;
          let i: number = 0;
          while (i++ < index) {
            current = current.next;
          }
          newNode.next = current;
          newNode.prev = current.prev;
          current.prev.next = newNode;
          current.prev = newNode;
        }
        this.length += 1;
      }
    }
  };

  public edit = <T>(index: number, data: T): void => {
    if (index < 0 || index > this.length) {
      console.log("Неверное значение индекса");
    } else {
      if (index < this.length / 2) {
        let i: number = 0;
        let current = this.head;
        while (i++ < index) {
          current = current.next;
        }
        current.data = data;
      } else {
        let i: number = this.length - 1;
        let previous: any = this.tail;
        while (index < i--) {
          previous = previous.prev;
        }
        previous.data = data;
      }
    }
  };

  public removeByIndex = (index: number): void => {
    if (index < 0 || index > this.length) {
      console.log("Неверное значение индекса");
    } else {
      if (this.length === 1) {
        this.head = null;
        this.tail = null;
      } else {
        if (index === 0) {
          this.head.next.prev = null;
          this.head = this.head.next;
        }
        else if (index === this.length - 1) {
          this.tail.prev.next = null;
          this.tail = this.tail.prev;
        } else {
          let i: number = 0;
          if (i < this.length / 2) {
            let current: any = this.head;
            while (i++ < index) {
              current = current.next;
            }
            current.prev.next = current.next;
            current.next.prev = current.prev;
          } else {
            let previous: any = this.tail;
            let i: number = this.length - 1;
            while (index < i--) {
              previous = previous.prev;
            }
            previous.prev.next = previous.next;
            previous.next.prev = previous.prev;
          }
        }
      }
      this.length -= 1;
    }
  };

  private getIndex = <T>(data: T): number => {
    let index: number = 0;
    let current: any = this.head;
    while (current) {
      if (current.data === data) {
        return index;
      }
      current = current.next;
      index++;
    }
    return -1;
  };

  public removeByValue = <T>(data: T): void => {
    let index: number = this.getIndex(data);
    if (index === -1) {
      console.log("Неверное значение");
    } else {
      this.length -= 1;
      this.removeByIndex(index);
    }
  };
}