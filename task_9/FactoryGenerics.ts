import {ConstructorType} from "./ConstructorType";

function logClassName (target: any, key: string, param: any): any {
  const result = param.value;
  param.value = function(...args: any[]): any {
      console.log("Object " + args[0].name + " created!");
      return result.apply(this, args);
  }
}

export class FactoryGenerics {
  @logClassName
  create<T>(object: ConstructorType<T>, ...args: any[]): T {
    return new object(...args);
  }
}