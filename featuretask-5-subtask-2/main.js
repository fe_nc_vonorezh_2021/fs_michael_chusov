class Brand {
  constructor(nameBrand) {
    this.nameBrand = nameBrand;
  }

  get getNameBrand() {
    return this.nameBrand;
  }

  set setBrand(brand) {
    this.Brand = brand;
  }
}

class Model extends Brand {
  constructor(nameBrand, nameModel, maxSpeed) {
    super(nameBrand);
    this.nameModel = nameModel;
    this.maxSpeed = maxSpeed;
  }

  set setModel(model) {
    this.nameModel = model;
  }

  set setSpeed(speed) {
    if (speed < 0) {
      console.log("The speed cannot be negative!");
    }
    this.maxSpeed = speed;
  }

  set setBrandModelSpeed(str) {
    [this.nameBrand, this.nameModel, this.maxSpeed] = str.split(" ");
  }

  get getBrandModelSpeed() {
    return "Brand: " + this.nameBrand + "\n" + "Model: " + this.nameModel + "\n" + "Speed: " + this.maxSpeed;
  }

  get getSpeed() {
    return 'Максимальная скорость = ' + this.maxSpeed;
  }
}

class Equipment extends Model {
  constructor(nameBrand, nameModel, maxSpeed, bodyType, cost, options) {
    super(nameBrand, nameModel, maxSpeed);
    this.bodyType = bodyType;
    this.cost = cost;
    this.options = options;
  }

  set setBodyType(bodyType) {
    this.bodyType = bodyType;
  }

  set setCost(cost) {
    this.cost = cost;
  }

  set setOptoins(options) {
    this.options = options;
  }

  get getPonts() {
    if (this.cost > 30000) {
      return 'this car is really expensive!';
    } else {
      return ':['
    }
  }

  get getCost() {
    return 'Cost = ' + this.cost;
  }

  numberOfOptions() {
    return "Количество выбранных опций = " + this.options.length;
  }
}

class Car extends Equipment {
  constructor(nameBrand, nameModel, maxSpeed, bodyType, cost, options, color) {
    super(nameBrand, nameModel, maxSpeed, bodyType, cost, options);
    this.color = color;
  }

  set setColor(color) {
    this.color = color;
  }

  get carInfo() {
    return 'CarInfo: \n' + this.nameBrand + ' ' + this.nameModel + '\n' + this.cost + '\n' + this.color;
  }
}

class BmwFactory {
  create(type) {
    if (type == 'X5') {
      return new Model('Bmw', type, 250);
    }
    if (type == 'X6') {
      return new Model('Bmw', type, 280);
    }
    if (type == 'M5') {
      return new Model('Bmw', type, 300);
    }
  }
}
// tests
//FactoryMetod
const factory = new BmwFactory;

const ModelBmwX5 = factory.create('X5');
console.log(ModelBmwX5);

const ModelBmwX6 = factory.create('X6');
console.log(ModelBmwX6);

ModelBmwX6.setSpeed = 290;//rewrite maxSpeed 
console.log(ModelBmwX6.getSpeed);
console.log(ModelBmwX6.getNameBrand);
////// 
let modelCar1 = new Model('Bmw', 'X5', 270);
console.log(modelCar1);
//using the constructor
let car1 = new Car('Mirsubishi', 'Outlander', '190', 'Univ', 90000, [1, 4, 5], 'blue');
console.log(car1.carInfo);
console.log(car1);
console.log(car1.getNameBrand);
let CarLexus = new Model;
//using setters
CarLexus.setBrandModelSpeed = "Lexus RX350 260";
console.log(CarLexus);
car1.setSpeed = -10;//"The speed cannot be negative!"
car1.setSpeed = 250;
car1.setCost = 100000;
//using getter
console.log(CarLexus.getBrandModelSpeed);
//methods 
console.log(car1.getPonts);
console.log(car1.getCost);
console.log(car1.numberOfOptions());

delete car1.bodyType;
console.log(car1.bodyType);
