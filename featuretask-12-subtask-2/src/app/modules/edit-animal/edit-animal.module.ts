import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditAnimalComponent } from './edit-animal.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    EditAnimalComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ]
})

export class EditAnimalModule { }
