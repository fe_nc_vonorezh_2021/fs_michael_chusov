export async function requestCoord() {
  let coords = {
    lat: 0,
    lon: 0
  };
  const cityName: string = (document.getElementById('input') as HTMLInputElement).value;
  const url: string = `https://photon.komoot.io/api/?lang=en&limit=1&q=${cityName}`;
  const response = await fetch(url);
  if (response.ok) {
    const data = await response.json();
    coords.lat = data.features[0].geometry.coordinates[1] as number;
    coords.lon = data.features[0].geometry.coordinates[0] as number;
  } else {
    alert('Wrong city');
    console.log("Wrong: " + response.status);
  }
  return coords;
}