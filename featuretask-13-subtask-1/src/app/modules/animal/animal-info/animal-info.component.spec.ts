import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AnimalInfoComponent } from './animal-info.component';
import { RouterTestingModule } from "@angular/router/testing";
import { HttpClientTestingModule } from '@angular/common/http/testing'; 

describe('AnimalInfoComponent', () => {
  let component: AnimalInfoComponent;
  let fixture: ComponentFixture<AnimalInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AnimalInfoComponent],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnimalInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
