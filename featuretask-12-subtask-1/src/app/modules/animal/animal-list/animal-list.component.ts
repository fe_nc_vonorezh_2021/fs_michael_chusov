import { Component, Input, OnInit } from '@angular/core';
import { Animal } from '../../../animal/animal';

@Component({
  selector: 'app-animal-list',
  templateUrl: './animal-list.component.html',
  styleUrls: ['./animal-list.component.less'],
})
export class AnimalListComponent implements OnInit {

  @Input()
  public animals: Animal[] = [];

  public _activeAnimalId: number;

  public _onAnimalSelect(id: number): void {
    this._activeAnimalId = id;
  }
}
