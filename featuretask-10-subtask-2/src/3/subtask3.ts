import { Observable } from "rxjs";

const { fromEvent, merge } = require("rxjs");
const body = document.getElementById('body');

const randomBodyColor = (): string => {
  let first: number = Math.floor(Math.random() * 255);
  let second: number = Math.floor(Math.random() * 255);
  let third: number = Math.floor(Math.random() * 255);
  let str: string = 'rgb(' + first + ', ' + second + ', ' + third + ')';
  return str;
};

const buttonVariables: any = {}
  , keys: string[] = ['streamButton1$', 'streamButton2$', 'streamButton3$'];

keys.forEach((v: string) => {
  buttonVariables[v] = fromEvent(document.getElementsByName('button'), 'click');
});

merge(buttonVariables.streamButton1$, buttonVariables.streamButton2$, buttonVariables.streamButton3$)
  .subscribe((value: Observable<Event>) => {
    body.style.background = randomBodyColor()
  });


