function partition(arr: number[], i: number, j: number): number {
  const pivot: number = arr[Math.floor((i + j) / 2)];
  while (i <= j) {
    while (arr[i] < pivot) {
      i++;
    }
    while (arr[j] > pivot) {
      j--;
    }
    if (i <= j) {
      [arr[i], arr[j]] = [arr[j], arr[i]];
      i++;
      j--;
    }
  }
  return i;
}

function quickSort(arr: number[], i: number, j: number): number[] {
  const index: number = partition(arr, i, j);
  if (arr.length > 1) {
    if (i < index - 1) {
      quickSort(arr, i, index - 1);
    }
    if (index < j) {
      quickSort(arr, index, j);
    }
  }
  return arr;
}

const n: number = 50;
let arr: number[] = [];
for (let i = 0; i < n; i++) {
  arr[i] = (Math.round(Math.random() * n));
}

console.log(arr);
const result = quickSort(arr, 0, n - 1);
console.log(result);
