import {List} from "./classes/list"

const list1: List = new List();

list1.append(8);
list1.append(10);
list1.append(11);
console.log(list1.getAll());
list1.edit(2, 10);
list1.insert(2, 11);
list1.insert(0, 4);
console.log(list1.getAll());
list1.removeByValue(10);
console.log(list1.getAll());
list1.edit(1, 9);
console.log(list1.getAll());
