const path = require('path')

module.exports = {
    entry: './task_4/config.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    }
}
